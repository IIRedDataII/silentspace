﻿using UnityEngine;

public abstract class Interactable : MonoBehaviour
{

    // game objects using this class or subclasses must have the interactable tag or else it will throw errors
    public abstract void Interact();

}
