﻿using UnityEngine;

public class SmallDoor : Interactable
{

    #region Constants

    private const float AttractFactor = 0.3f;

    #endregion
    
    #region Unity Variables

    [SerializeField] private AudioClip soundOpen;
    [SerializeField] private AudioClip soundClose;
    
    #endregion
    
    #region Private Variables
    
    private AudioSource audioSource;
    private Animator animator;
    private bool state;
    
    #endregion

    private void Start()
    {

        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();

    }

    public override void Interact() {
        
        state = !state;
        
        animator.SetBool(Animator.StringToHash("open"), state);
        audioSource.clip = state ? soundOpen : soundClose;
        
        Utils.instance.MakeSound(audioSource, true, AttractFactor);
        
    }
    
}
