﻿using UnityEngine;
using UnityEngine.Events;

public class ButtonInteractable : Interactable
{
    
    #region Unity Variables
    
    [SerializeField] private UnityEvent pressEvent;
    [SerializeField] private bool oneShot;

    #endregion

    #region Private Variables
    
    private bool used;

    #endregion
    
    public override void Interact()
    {
        if (used && oneShot)
            return;
        used = true;
        
        pressEvent.Invoke();
    }
    
}
