﻿using UnityEngine;

public class GravityGunInteractable : Interactable
{
    
    #region Unity Variables

    [SerializeField] private GravityGunUsable gravityGunScript;
    
    #endregion
    
    public override void Interact()
    {
        
        gravityGunScript.GetGravityGun();
        Destroy(gameObject);
        
    }
    
}
