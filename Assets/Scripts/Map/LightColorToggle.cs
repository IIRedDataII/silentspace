﻿using UnityEngine;

public class LightColorToggle : MonoBehaviour
{

    #region Constants
    
    private readonly Color positive = new Color(0f, 1f, 0f, 1f);
    private readonly Color negative = new Color(1f, 0.5f, 0f, 1f);
    
    #endregion

    #region Private Variables
    
    private Light lightSource;
    private bool state;

    #endregion

    private void Start()
    {
        lightSource = GetComponent<Light>();
    }

    // called by unity
    public void Toggle()
    {
        state = !state;
        lightSource.color = state ? positive : negative;
    }
    
}
