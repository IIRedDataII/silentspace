﻿public static class GameData
{

    public static bool postSecondDoor;
    public static bool hasHelmet;
    public static bool airDrained;
    public static bool airBeingDrained;

    public static void Reset()
    {
        postSecondDoor = false;
        hasHelmet = false;
        airDrained = false;
        airBeingDrained = false;
    }

}
