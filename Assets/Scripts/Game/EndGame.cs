﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    
    #region Singleton Pattern

    public static EndGame instance;
    
    private void Awake()
    {
        instance = this;
    }

    #endregion
    
    #region Unity Variables
    
    [SerializeField] private AudioClip winMusicClip;
    [SerializeField] private AudioSource musicAudioSource;
    [SerializeField] private GameObject gameOverImage;
    [SerializeField] private TorchUsable torchScript;
    [SerializeField] private FPC fpcScript;
    
    #endregion
    
    #region Public Functions
    
    public void WinGame()
    {
        fpcScript.gravity = 0f;
        musicAudioSource.Stop();
        musicAudioSource.PlayOneShot(winMusicClip);
        StartCoroutine(WinDelay());
    }
    
    public void LoseGame(string deathMessage)
    {
        Debug.Log("You lost! " + deathMessage);
        StartCoroutine(LoseDelay());
    }

    #endregion

    #region Coroutines
    
    private IEnumerator WinDelay()
    {
        yield return new WaitForSeconds(20f);
        SceneManager.LoadScene("Menu");
    }
    
    private IEnumerator LoseDelay()
    {
        gameOverImage.SetActive(true);
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene("Menu");
    }
    
    #endregion
    
}
