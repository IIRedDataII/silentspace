﻿using UnityEngine;

public class CeilingCheck : MonoBehaviour
{
    
    [SerializeField] private FPC fpc;
    
    private void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("Player") && !other.CompareTag("Item"))
        {
            fpc.BonkIntoCeiling();
        }
    }
    
}
