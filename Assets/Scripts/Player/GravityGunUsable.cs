﻿using UnityEngine;

public class GravityGunUsable : MonoBehaviour
{
    
    #region Constants
    
    private const float GrabDistance = 7f;
    private readonly Color grabbableColor = new Color(0.15f, 0f, 0.1f);
    
    #endregion

    #region Unity Variables
    
    [SerializeField] private Camera playerCamera;
    [SerializeField] private Transform objectHolder;
    [SerializeField] private GameObject gunModel;
    [SerializeField] private ParticleSystem ray;
    
    #endregion
    
    #region Private Variables

    private GravityGunSounds soundsScript;
    private RaycastHit oldHit;
    private RaycastHit currentHit;
    private Rigidbody grabbedRigidbody;
    private Color originalColor;
    private bool hasGravityGun;
    private bool canGrab;

    #endregion

    private void Start()
    {

        #region Variable Initialization
        
        soundsScript = GetComponent<GravityGunSounds>();
        
        #endregion
        
    }

    private void Update()
    {

        #region Select Gun

        if (hasGravityGun)
        {
            if (Input.mouseScrollDelta.y > 0)
                SetActive(true);
            else if (Input.mouseScrollDelta.y < 0)
                SetActive(false);
        }

        #endregion
        
        #region Grabbing Function
        
        if (canGrab)
        {
            
            // move grabbed object around
            if (grabbedRigidbody)
            {
                grabbedRigidbody.MovePosition(objectHolder.transform.position);

                // release grabbed object
                if (Input.GetKeyDown(KeyCode.E))
                {
                    ReleaseObject();
                    oldHit = new RaycastHit();
                }

            }
            
            // target something (currentHit is not null in here)
            //Debug.DrawLine(playerCamera.transform.position, playerCamera.transform.position + playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)).direction * GrabDistance, Color.yellow);
            else if (Physics.Raycast(playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)), out currentHit, GrabDistance) && currentHit.collider.CompareTag("Grabbable"))
            {
                
                if (!oldHit.collider || !oldHit.collider.Equals(currentHit.collider))
                {
                    UncolorObject(oldHit);
                    
                    Material currentMaterial = currentHit.collider.gameObject.GetComponent<MeshRenderer>().material;
                    originalColor = currentMaterial.GetColor(Shader.PropertyToID("_EmissionColor"));
                    currentMaterial.SetColor(Shader.PropertyToID("_EmissionColor"), grabbableColor);
                    oldHit = currentHit;
                }

                // grab current object
                if (Input.GetKeyDown(KeyCode.E))
                {
                    UncolorObject(currentHit);
                    
                    grabbedRigidbody = currentHit.rigidbody;
                    if (grabbedRigidbody)
                    {
                        // set distance from player
                        Vector3 localPosition = objectHolder.localPosition;
                        objectHolder.localPosition = new Vector3(localPosition.x, localPosition.y, (grabbedRigidbody.transform.position - playerCamera.transform.position).magnitude);
                        
                        // ray & sound
                        soundsScript.PlayGrabSound();
                        ray.Play();
                        
                        // disable collider
                        grabbedRigidbody.gameObject.GetComponent<Collider>().enabled = false;
                        
                        // "disable" rigidbody
                        grabbedRigidbody.isKinematic = true;
                    }
                }

            }
            
            else
            {
                // uncolor old object
                UncolorObject(oldHit);
                oldHit = new RaycastHit();
            }
            
        }
        else
        {
            // release grabbed object & uncolor current object
            UncolorObject(currentHit);
            oldHit = new RaycastHit();
            ReleaseObject();
        }
        
        #endregion
        
    }
    
    #region Helper Functions
    
    private void SetActive(bool active)
    {
        canGrab = active;
        gunModel.SetActive(active);
        GetComponent<GravityGunSounds>().humSoundSource.mute = !active;
    }

    private void UncolorObject(RaycastHit hit)
    {
        if (hit.collider && hit.collider.CompareTag("Grabbable"))
            hit.collider.gameObject.GetComponent<MeshRenderer>().material.SetColor(Shader.PropertyToID("_EmissionColor"), originalColor);
    }
    
    private void ReleaseObject()
    {
        if (grabbedRigidbody)
        {
            
            // ray & sound
            soundsScript.ResetHumSoundSource();
            soundsScript.PlayReleaseSound();
            ray.Stop();

            // enable collider
            grabbedRigidbody.gameObject.GetComponent<Collider>().enabled = true;  
            
            // "enable" rigidbody
            grabbedRigidbody.isKinematic = false;
            
            grabbedRigidbody = null;
        }
    }
    
    #endregion
    
    #region Public Functions
    
    public void GetGravityGun()
    {
        hasGravityGun = true;
        SetActive(true);
    }
    
    public void LoseGravityGun()
    {
        hasGravityGun = false;
        SetActive(false);
        soundsScript.StopAllCoroutines();
    }

    public GameObject GetGrabbedObject()
    {
        return grabbedRigidbody ? grabbedRigidbody.gameObject : null;
    }
    
    #endregion
    
}
