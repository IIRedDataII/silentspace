﻿using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    
    [SerializeField] private FPC fpc;
    
    private void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("Player"))
        {
            fpc.onGround = true;
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Player"))
        {
            fpc.onGround = false;
        }
    }
    
}
