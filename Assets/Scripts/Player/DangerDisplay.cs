﻿using UnityEngine;

public class DangerDisplay : MonoBehaviour
{
    
    #region Constants
    
    private readonly int emission = Shader.PropertyToID("_EmissionColor");
    private readonly Color[] colors =
    {
        new Color(0f, 1f, 0f), new Color(2.25f/9f, 1f, 0f), new Color(4.5f/9f, 1f, 0f),
        new Color(6.75f/9f, 1f, 0f), new Color(1f, 1f, 0f), new Color(1f, 6.75f/9f, 0f),
        new Color(1f, 4.5f/9f, 0f), new Color(1f, 2.25f/9f, 0f), new Color(1f, 0f, 0f),
    };
    
    #endregion
    
    #region Unity Variables
    
    [SerializeField] private Renderer[] displays;

    #endregion

    #region Private Variables

    private Material[] materials = new Material[9];
    private int oldDangerLevel;
    
    #endregion
    
    private void Start()
    {
        
        #region Variable Initialization
        
        for (int i = 0; i < 9; i++)
            materials[i] = displays[i].material;
        oldDangerLevel = 0;

        #endregion

    }
    
    private void Update()
    {

        #region Color

        float attractionValue = Alien.instance.GetAttractionValue();
        int dangerLevel;
        if (attractionValue >= 9f/3f)
            dangerLevel = 9;
        else if (attractionValue >= 8f/3f)
            dangerLevel = 8;
        else if (attractionValue >= 7f/3f)
            dangerLevel = 7;
        else if (attractionValue >= 6f/3f)
            dangerLevel = 6;
        else if (attractionValue >= 5f/3f)
            dangerLevel = 5;
        else if (attractionValue >= 4f/3f)
            dangerLevel = 4;
        else if (attractionValue >= 3f/3f)
            dangerLevel = 3;
        else if (attractionValue >= 2f/3f)
            dangerLevel = 2;
        else if (attractionValue >= 1f/3f)
            dangerLevel = 1;
        else
            dangerLevel = 0;
        
        if (dangerLevel != oldDangerLevel)
        {
            oldDangerLevel = dangerLevel;

            for (int i = 0; i < 9; i++)
            {
                if (i > dangerLevel-1)
                    materials[i].SetColor(emission, Color.white);
                else
                    materials[i].SetColor(emission, colors[i]);
            }
        }
        
        #endregion
        
    }
    
}
