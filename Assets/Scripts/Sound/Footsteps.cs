﻿using System.Collections;
using UnityEngine;

public class Footsteps : MonoBehaviour
{

    #region Constants

    private const float AttractFactor = 0.4f;
    private const float Speed = 2f;

    #endregion

    #region Unity Variables
    
    [SerializeField] private AudioClip[] footsteps;
    [SerializeField] private FPC fpcScript;

    #endregion

    #region Private Variables

    private AudioSource audioSource;
    private bool active = true;
    private int oldIndex = -1;

    #endregion
    
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        
        if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)) && active && fpcScript.onGround)
        {
            int index;
            do
            {
                index = Random.Range(0, footsteps.Length);
            } while (index == oldIndex);
            oldIndex = index;
            
            audioSource.clip = footsteps[index];
            Utils.instance.MakeSound(audioSource, false, AttractFactor);
            
            active = false;
            StartCoroutine(Reactivate());
        }
        
    }

    private IEnumerator Reactivate()
    {
        yield return new WaitForSeconds(1f / Speed);
        active = true;
    }
    
}
